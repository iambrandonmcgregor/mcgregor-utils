// Import our external dependencies.
import { writable, get } from "svelte/store";
import { urlToData, formatUrl } from "./url-utils";

// Instantiate the individual stores, elevating their set functions.
export const url = writable("");
export const route = writable([]);
export const route0 = writable("");
export const route1 = writable("");
export const route2 = writable("");
export const route3 = writable("");
export const route4 = writable("");
export const route5 = writable("");
export const route6 = writable("");
export const queryParameters = writable({});

// Expose a function to start hash based routing.
export function startHashRouting () {
  refreshRoute();
  window.addEventListener("hashchange", refreshRoute);
}

// Provide a function to refresh the app's route based on the nav bar.
export function refreshRoute() {
  const routeData = chopTheHashUp();
  route.set(routeData.route);
  route0.set(routeData.route0);
  route1.set(routeData.route1);
  route2.set(routeData.route2);
  route3.set(routeData.route3);
  route4.set(routeData.route4);
  route5.set(routeData.route5);
  route6.set(routeData.route6);
  url.set(routeData.url);
  queryParameters.set(routeData.queryParameters);
}

// Provide a function to update query parameters programmatically.
export function updateQueryParameters(newQPs = {}, replace = true, state = null) {
  const $route = get(route);
  const $queryParameters = get(queryParameters);
  const newState = `${window.location.pathname}#/${formatUrl({ newQPs, $route, $queryParameters })}`;
  if (replace) {
    window.history.replaceState(state, null, newState);
    return refreshRoute();
  }
  window.location = newState;
}

// Private Functions
// …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …  …|
function chopTheHashUp() {
  return urlToData(window.location.hash, true);
}