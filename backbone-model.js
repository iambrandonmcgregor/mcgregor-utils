
// Include our external dependencies.
const Backbone = require('backbone');

// Extend the default BB Model functionality. 
const Model = Backbone.Model.extend({

    // Overload this.fetch() to auto-debounce multiple fetch calls and to track loading state in the model.
    fetch (options={}, moreOptions={}) { /* arguments are passed through to the ancestor fetch call. */

        // Cache a reference to the current context and some model data.
        const self = this;
        let { loading } = this.attributes;
        const { debounce=true } = moreOptions;

        // Debounce multiple fetch calls.
        if (debounce && loading)
            return loading;

        // Call the ancestor Model.fetch() and cache the results.
        loading = Backbone.Model.prototype.fetch.apply(this, arguments);

        // Once the http request returns, track the `loading` and `initialization` states.
        loading = loading.then(function onFetchSuccess_resetLoadingState () {
            self.set({
                loading: false,
                initialized: true,
                httpError: null,
            });
        }).catch(function onFetchError (error = '(No Error Provided)') {
            self.set({
                loading: false,
                initialized: true,
                httpError: error.responseText || error,
            });
            return error;
        });

        // Store the jqXHR promise in a truthy `loading` property.
        this.set({loading});
        return loading;
    },

    reset (newData, options) {
        this.clear({silent:true});
        return this.set({
            ...newData,
            ...this.defaults(),
        }, options);
    },

});

// Export our Model.
module.exports = Model;