
// Import our external dependencies.
const defaults = require('lodash/defaults');

let queryParameters = {};

/*
    This function was designed as a Svelte helper. Notice the destructed single POJO argument.
    This generates a new URL given the current url and specified replacements. This is useful for 
    live-updating stateful links in your UI without too much if/else complexity.
*/
function formatUrl ({ $route, $queryParameters, zero, one, two, three, four, five, newQPs = {} }) {

    //TODO: This could stand to be memory optimized. Too much string concatination.

    // Build the main portion of the URL.
    const newRoute = [zero,one,two,three,four,five];
    let url = '';
    for (let i=0; (i<$route.length || i<newRoute.length); i++) {
        if (i > 0)
            url += '/';
        url += newRoute[i] === null
            ? '*'
            : newRoute[i] || $route[i] || '*';
    }
    while (url[url.length-1] === '*') {
        url = url.slice(0, -2);
    }

    // Add the query parameters.
    const queryParameters = defaults(newQPs, $queryParameters);
    let first = true;
    for (var prop in queryParameters) {
        if (queryParameters[prop]) {
            url += first ? '?' : '&';
            first = false;
            url += `${prop}=${queryParameters[prop]}`;
        }
    }

    return url;
}

function urlToData (routeStr = '', includeFlatProps = false) {

    // Remove leading # and / if they're present.
    if (routeStr[0] === '#')
        routeStr = routeStr.substring(1);
    if (routeStr[0] === '/')
        routeStr = routeStr.substring(1);
    routeStr.trim();

    const splitQuery = routeStr.split('?');
    const url = splitQuery[0];
    const route = url.split('/');

    // Reset the query parameters.
    for (var prop in queryParameters) {
        queryParameters[prop] = null;
    }
    
    // Prepare the query parameters
    if (splitQuery[1]) {
        const paramStrs= splitQuery[1].split('&'); // [ 'awesome=true', 'pizza=yummy' ]
        for (let i=0, l=paramStrs.length; i<l; i++) {
            const name = decodeURIComponent(paramStrs[i].split('=')[0]);
            const value = decodeURIComponent(paramStrs[i].slice(name.length+1));
            queryParameters[name] = value || true;
        }
    }
    const urlData = { url, route, queryParameters };

    if (includeFlatProps) {
        urlData.route0 = urlData.route1 = urlData.route2 = urlData.route3 = urlData.route4 = urlData.route5 = urlData.route6 = false;
        route.forEach((chunk, i) => urlData[`route${i}`] = chunk);
    }

    return urlData;
}

// End by exposing our functions for import.
module.exports = {
    formatUrl,
    urlToData,
}